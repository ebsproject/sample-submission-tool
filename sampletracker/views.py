from django.contrib.auth.decorators import login_required
from django.shortcuts import render, get_object_or_404
from django.http import HttpResponse, HttpResponseRedirect
from django.core.files.storage import FileSystemStorage
from django.contrib.auth.mixins import LoginRequiredMixin, UserPassesTestMixin
from django.views.generic import (
    ListView,
    DetailView,
    CreateView,
    UpdateView,
    DeleteView
)
from django.urls import reverse, reverse_lazy
from .models import Project, Order, Service, Species, Sample
from .forms import OrderForm, UploadFileForm


def home(request):
    context = {
        'projects': Project.objects.all()
    }
    return render(request,'sampletracker/home.html',context)

def about(request):
    return render(request, 'sampletracker/about.html')

def services(request):
    context = {
        'services': Service.objects.all()
    }
    return render(request, 'sampletracker/services_list.html',context)

class ProjectListView(LoginRequiredMixin, ListView):
    model = Project
    template_name = 'sampletracker/home.html'
    context_object_name = 'projects'
    ordering = ['date_created']

class ProjectDetailView(DetailView):
    model = Project
    template_name = 'sampletracker/project_detail.html'

class ProjectCreateView(LoginRequiredMixin,CreateView):
    model = Project
    fields = ['name','description']
    success_url = "/"

    def form_valid(self, form):
        form.instance.user = self.request.user
        return super().form_valid(form)

class ProjectUpdateView(LoginRequiredMixin, UserPassesTestMixin, UpdateView):
    model = Project
    template_name = 'sampletracker/project_updateform.html'
    fields = ['name','description']

    def form_valid(self, form):
        form.instance.user = self.request.user
        return super().form_valid(form)

    def test_func(self):
        project = self.get_object()
        if self.request.user == project.user:
            return True
        return False

class ProjectDeleteView(LoginRequiredMixin, UserPassesTestMixin,DeleteView):
    model = Project
    success_url = "/"

    def test_func(self):
        project = self.get_object()
        if self.request.user == project.user:
            return True
        return False

class OrderListView(LoginRequiredMixin,UserPassesTestMixin, ListView):
    model = Order
    template_name = 'sampletracker/request_list.html'
    context_object_name = 'orders'

    def test_func(self):
        order = self.get_object()
        if self.request.user == order.user:
            return True
        return False

    def get_queryset(self, *args, **kwargs):
        return OrderListView.objects.filter(project=self.kwargs['pk'])

class OrderCreateView(LoginRequiredMixin, UserPassesTestMixin,CreateView):
    model = Order
    fields = ['name','species', 'vendor', 'service','plate_num','project']
    #success_url = "/"

    def form_valid(self, form):
        form.instance.pk = Project.objects.get(pk=self.kwargs.get('id'))
        return super().form_valid(form)

    def test_func(self):
        order = self.get_object()
        if self.request.user == order.user:
            return True
        return False

def list_order(request):
    context = {
        'orders': Order.objects.all()
    }
    return render(request,'sampletracker/request_list.html',context)
    #return render(request,'sampletracker/order_list.html',context)

def create_order(request):
    if request.method == 'POST':
        form = OrderForm(request.POST)
        if form.is_valid():
            species = form.cleaned_data['species']
            vendor = form.cleaned_data['vendor']
            service = form.cleaned_data['service']
            platenum = form.cleaned_data['plate_num']
            project = form.cleaned_data['project']
            # clean data first
            form.save()
            return HttpResponseRedirect('/')
    else:
        form = OrderForm()
    return render(request,'sampletracker/order_form.html',{'form':form})

## delete an order in the list

class OrderDeleteView(LoginRequiredMixin, UserPassesTestMixin,DeleteView):
    model = Order
    template_name = 'sampletracker/order_confirm.html'
    success_url="/"
    
    def test_func(self):
        order= self.get_object()
        if self.request.project == order.project:
            return True
        return False
        
class OrderDetailView(LoginRequiredMixin, UserPassesTestMixin,DetailView):
    model = Order
    template_name = 'sampletracker/order_detail.html'


class OrderUpdateView(LoginRequiredMixin, UserPassesTestMixin,DeleteView):
    model = Order
    success_url = "/"

    def form_valid(self, form):
        form.instance.user = self.request.user
        return super().form_valid(form)

    def test_func(self):
        order = self.get_object()
        if self.request.user == order.user:
            return True
        return False

def upload_file(request):
    if request.method == 'POST':
        form = UploadFileForm(request.POST, request.FILES)
        if form.is_valid():
            handle_uploaded_file(request.FILES['file'])
            return HttpResponseRedirect('/success/url/')
    else:
        form = UploadFileForm()
    return render(request, 'sampletracker/sampleslist_form.html', {'form': form})

class DataView(UploadFileForm):
    template_name = 'sampletracker/sampleslist_form.html'
    form_class = UploadFileForm
    success_url = '/order/'

    def form_valid(self, form):
        form.process_data()
        return super().form_valid(form)

def create_samplelist(request):
    ## 
    ## upload file in csv, tsv or xlsx
    ## parse file
    ## generate UUIDs and check sample ID name
    context = {}
    if request.method == 'POST':
        form = UploadFileForm(request.POST, request.FILES)
        sampleFile = request.FILES['samplelist']
        sampleFileName = sampleFile.name
        # save file 
        fs = FileSystemStorage()
        name = fs.save(sampleFile.name,sampleFile)
        context['url'] = fs.url(name)
        if form.is_valid():
            # save form
            form.save()
            handle_uploaded_file(sampleFile)
            return HttpResponseRedirect('sampletracker/samplelist.html')
    else:
        form = UploadFileForm()
    return render(request, 'sampletracker/sampleslist_form.html', {'form': form})

def handle_uploaded_file(f):
    ouputFile = open('sampletracker/outFileTmp.txt', 'w')
    if not f.multiple_chunks():
        outputFile.write(f.read())
    else:
        for chunk in f.chunks():
            outputFile.write(chunk)
    outputFile.close()


def list_samples(request):
    samples = Sample.objects.all()
    context = {
        'samples': samples
    }
    return render(request,'sampletracker/sampleslist.html',context)