from django.urls import path
from django.conf import settings
from django.conf.urls.static import static
from .views import (
    ProjectListView,
    ProjectDetailView,
    ProjectCreateView,
    ProjectUpdateView,
    ProjectDeleteView,
    OrderCreateView,
    OrderListView,
    OrderDeleteView,
    OrderUpdateView,
    OrderDetailView
)
from . import views

urlpatterns = [
    #path('', views.home, name='sampletracker-home'), 
    path('about/', views.about, name='sampletracker-about'),
    path('services/', views.services, name='sampletracker-services'),
    path('', ProjectListView.as_view(), name='sampletracker-home'),
    path('project/<int:pk>/', ProjectDetailView.as_view(), name='project-detail'),
    path('project/new/', ProjectCreateView.as_view(), name='project-create'),
    path('project/<int:pk>/update/', ProjectUpdateView.as_view(), name='project-update'),
    path('project/<int:pk>/delete/', ProjectDeleteView.as_view(), name='project-delete'),
    path('project/order/', views.list_order, name='order-list'),
    path('<int:pk>/delete/', OrderDeleteView.as_view(), name="order-delete"),
    path('<int:pk>/update/', OrderUpdateView.as_view(), name="order-update"),
    #path('order/new/', OrderCreateView.as_view(), name="order-create"),
    path('order/new/', views.create_order, name='order-create'),
    path('order/samplelist', views.create_samplelist, name='create-sampleslist'),
    path('order/allsamples', views.list_samples, name='list-sampleslist'),
]

# Not for production mode
if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL,document_root=settings.MEDIA_ROOT)