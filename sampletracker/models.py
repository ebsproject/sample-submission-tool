from django.db import models
from django.utils import timezone
from django.contrib.auth.models import User
from django.urls import reverse
from django.forms import ModelForm


class Project(models.Model):
    name = models.CharField(max_length=50)
    description = models.CharField(max_length=100)
    date_created = models.DateTimeField(default=timezone.now)
    date_modified = models.DateTimeField(default=timezone.now)
    user = models.ForeignKey(User, on_delete=models.CASCADE)

    def __str__(self):
        return self.name

    # get url of route
    # get reverse fxn
    def get_absolute_url(self):
        return reverse('project-detail', kwargs={'pk':self.pk})

    class meta:
        ordering = ['name']

class ControlledVocabulary(models.Model):
    name = models.CharField(max_length=20)
    definition = models.CharField(max_length=100)
    defined_by = models.ForeignKey(User, on_delete=models.CASCADE,default=1)
    date_created = models.DateTimeField(default=timezone.now)
    date_created = models.DateTimeField(default=timezone.now) 

class CVterm(models.Model):
    name = models.CharField(max_length=50)
    cv_id = models.ForeignKey(ControlledVocabulary,on_delete=models.CASCADE)
    definition = models.CharField(max_length=100)
    date_created = models.DateTimeField(default=timezone.now)
    date_created = models.DateTimeField(default=timezone.now) 

    def __str__(self):
        return self.name

## Allow admins to add new Species 
class Species(models.Model):
    name = models.CharField(max_length=100)
    display_name = models.CharField(max_length=50)
    species_type = models.ForeignKey(CVterm,on_delete=models.CASCADE,default=0)
    date_created = models.DateTimeField(default=timezone.now)
    date_created = models.DateTimeField(default=timezone.now) 

    def __str__(self):
        return self.display_name

class Service(models.Model):
    name = models.CharField(max_length=50)
    description = models.CharField(max_length=100)
    service_type = models.ForeignKey(CVterm,on_delete=models.CASCADE)
    is_available = models.BooleanField(default=True)
    date_created = models.DateTimeField(default=timezone.now)
    date_created = models.DateTimeField(default=timezone.now)

    def __str__(self):
        return self.name 

class Contact(models.Model):
    name = models.CharField(max_length=100)
    contact_type = models.ForeignKey(CVterm,on_delete=models.CASCADE)
    address = models.CharField(max_length=200)
    email_address = models.EmailField(max_length=50)
    date_created = models.DateTimeField(default=timezone.now)
    date_created = models.DateTimeField(default=timezone.now) 

    def __str__(self):
        return self.name

class Order(models.Model):
    id = models.IntegerField(primary_key=True,serialize=True)
    plate_num = models.IntegerField()
    species = models.ForeignKey(Species,on_delete=models.CASCADE, related_name='speciesset')
    project = models.ForeignKey(Project, on_delete=models.CASCADE)
    vendor = models.ForeignKey(Contact, on_delete = models.CASCADE,related_name = 'vendorset')
    service = models.ForeignKey(Service, on_delete = models.CASCADE, related_name = 'serviceset')
    date_created = models.DateTimeField(default=timezone.now)
    date_modified = models.DateTimeField(default=timezone.now) 

    def __str__(self):
        return self.id

    def get_absolute_url(self):
        return reverse('order-detail', kwargs={'pk':self.pk})

class Sample(models.Model):
    id = models.IntegerField(primary_key=True)
    name = models.CharField(max_length=30)
    subject_barcode = models.IntegerField()
    plate_id = models.CharField(max_length=50, default='null')
    plate_barcode = models.CharField(max_length=50, default='null')
    # location = models.CharField(max_length=3,default='A01')
    # status = models.IntegerField()
    # date_submitted = models.DateTimeField(default=timezone.now)
    # date_modified = models.DateTimeField(default=timezone.now)

    def __str__(self):
        return self.name

class Profile(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    type = models.CharField(max_length =10, default="customer")