# Generated by Django 3.1.5 on 2021-05-12 05:31

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('sampletracker', '0020_auto_20210512_1325'),
    ]

    operations = [
        migrations.AddField(
            model_name='sample',
            name='location',
            field=models.CharField(default='null', max_length=3),
        ),
    ]
