# Generated by Django 3.1.5 on 2021-05-12 03:56

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('sampletracker', '0018_auto_20210421_0956'),
    ]

    operations = [
        migrations.CreateModel(
            name='Sample',
            fields=[
                ('id', models.IntegerField(primary_key=True, serialize=True)),
                ('name', models.CharField(max_length=30)),
                ('barcode_id', models.IntegerField(serialize=True, default=1000000001 )),
            ]
        ),
    ]
