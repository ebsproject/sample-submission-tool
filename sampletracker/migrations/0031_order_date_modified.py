# Generated by Django 3.1.5 on 2021-05-18 07:13

from django.db import migrations, models
import django.utils.timezone


class Migration(migrations.Migration):

    dependencies = [
        ('sampletracker', '0030_auto_20210518_1402'),
    ]

    operations = [
        migrations.AddField(
            model_name='order',
            name='date_modified',
            field=models.DateTimeField(default=django.utils.timezone.now),
        ),
    ]
