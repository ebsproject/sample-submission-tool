from django.apps import AppConfig


class SampletrackerConfig(AppConfig):
    name = 'sampletracker'