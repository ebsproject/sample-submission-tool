from django import forms
from .models import Order

class SpeciesSelect(forms.Select):
    def create_option(self, name, value, label, selected, index, subindex=None, attrs=None):
        option = super().create_option(name, value, label, selected, index, subindex, attrs)
        if value:
            option['attrs']['name'] = value.instance.name
        return option

class ServiceSelect(forms.Select):
    def create_option(self, name, value, label, selected, index, subindex=None, attrs=None):
        option = super().create_option(name, value, label, selected, index, subindex, attrs)
        if value:
            option['attrs']['name'] = value.instance.name
        return option

class VendorSelect(forms.Select):
    def create_option(self, name, value, label, selected, index, subindex=None, attrs=None):
        option = super().create_option(name, value, label, selected, index, subindex, attrs)
        if value:
            option['attrs']['name'] = value.instance.name
        return option

class OrderForm(forms.ModelForm):
    class Meta:
        model = Order
        fields = ['species','vendor','service','plate_num']
        widgets = {'species':SpeciesSelect,'vendor':VendorSelect,'service':ServiceSelect}

class UploadFileForm(forms.Form):
    title = forms.CharField(max_length=50)
    file = forms.FileField()

    def process_form(self):
        f = io.TextIOWrapper(self.cleaned_data['file'].file)
        reader = csv.DictReader()