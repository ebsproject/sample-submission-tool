from django.contrib import admin
from .models import Project, Order, Sample, ControlledVocabulary, CVterm, Contact, Species, Service

# Register your models here.
admin.site.register(Project)
admin.site.register(Order)
admin.site.register(Sample)
admin.site.register(ControlledVocabulary)
admin.site.register(CVterm)
admin.site.register(Species)
admin.site.register(Service)