# README #


The SampleTracker is a web application tool for optimizing sample  submissions and tracing them in the submission pipeline. It allows users to submit samples and later on, send these sample list to the genotyping vendors. 

Sample ids, sample barcodes as well as plate barcodes are system-generated and are tracked within the application.

This project is currently deployed [here] (http://eib-sample-tracker.ebsproject.org/)



### What is this repository for? ###

* SampleTracker
* Version: 1.0.0
* Packages needed:

        certifi==2020.12.5
        cffi==1.14.5
        chardet==4.0.0
        cryptography==3.4.6
        Django==3.1.5
        django-allauth==0.44.0
        django-crispy-forms==1.11.1
        idna==2.10
        ldap3==2.9
        oauthlib==3.1.0
        Pillow==8.1.2
        psycopg2==2.8.6
        pyasn1==0.4.8
        pycparser==2.20
        PyJWT==2.0.1
        python3-openid==3.2.0
        pytz==2020.5
        requests==2.25.1
        requests-oauthlib==1.3.0
        sqlparse==0.4.1
        urllib3==1.26.3

See requirements.txt for complete list


### How do I get set up? ###

* Quick Start:

    1. Install Python. This application requires Python >= 3.6. See requirements.txt for list of packages.

    2. Clone repository through Bitbucket:

            git clone https://bitbucket.org/ebsproject/sample-submission-tool/src/master/ m3tracker

    3. To run the application: 

            python manage.py runserver 8080

        The application will run on http://127.0.0.1:8080 


### Database Set-up:

    A. Installing postgresql. (This application uses Postgresql 13)

         Download and install postgres. See link for reference: https://www.postgresql.org/download/

    B. Set-up Django admin/superuser:

        To login to the admin site, you will need to create a superuser. Go to the application HOME_DIRECTORY and run manage.py

        $ python manage.py createsuperuser
        
        Username: <>
        Passowrd: 
        
    C. Database Setting:
    
     Initially, any Django project will use sqlite as default. This application however, uses postgresql. Change this by:
     
     - Add username and database name in settings.py
     
     - ENGINE must be set to : 'django.db.backends.postgresql'
     
     - NAME must be set to 'sampletrackerdb'
     
     - HOST set to 'localhost'
     
     - PORT set to '5400'

     For more database settings, see: https://docs.djangoproject.com/en/3.2/ref/settings/#databases

     D. Social Account Authentication:

     -  Make sure that 'allauth' is installed and added to INSTALLED_APPS (see settings.py)

     - Add the following to settings.py:

        SOCIALACCOUNT_PROVIDERS = {
                'google': {
                        'SCOPE': [
                        'profile',
                        'email',
                        ],
                        'AUTH_PARAMS': {
                        'access_type': 'online',
                        }
                }
         }

### Who do I talk to? ###

* This repository is owned by the Excellence in Breeding Module 3.
* For inquiries, email: Venice Juanillas (v.juanillas@irri.org, venicemargarettejuanillas@gmail.com)