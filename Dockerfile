From python:3

WORKDIR /data

COPY requirements.txt requirements.txt

RUN pip3 install -r requirements.txt

COPY . sample-submission-tool/
COPY settings.py sample-submission-tool/m3tracker/settings.py

EXPOSE 8080
STOPSIGNAL SIGTERM

CMD python ./sample-submission-tool/manage.py runserver 0.0.0.0:8080

## configure database
## Run postgresql
#RUN pip install psycopg2

## create superuser
## load psql dump