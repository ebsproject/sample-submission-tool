#!/usr/bin/env bash
# start-server.sh
if [ -n "$DJANGO_SUPERUSER_USERNAME" ] && [ -n "$DJANGO_SUPERUSER_PASSWORD" ] ; then
    (cd m3tracker; python manage.py createsuperuser --no-input)
fi

(cd m3tracker; python manage.py runserver 8080)